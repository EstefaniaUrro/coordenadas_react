import React, { Component } from "react";

export default class Formulario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nombre: null,
      situ: false,
    };
  }
  procesaInput = (e) => {
    console.log(e.target.value);
    this.setState({ nombre: e.target.value });
  };
  muestra = () => this.setState({ situ: true });
  limpia = () => this.setState({ situ: false });

  render() {
    return this.state.situ ? (
      <div>
        <h1>Bienvenid@ {this.state.nombre}</h1>
        <br />
        <button
          type="submit"
          className="btn btn-dark"
          onClick={this.limpia}>Limpiar</button>
      </div>
    ) : (
        <div className="container">
          <input
            className="form-control"
            type="text"
            onChange={(e) => {
              this.procesaInput(e);
            }}
          />
          <button
            type="submit"
            className="btn btn-primary"
            onClick={this.muestra}>Enviar</button>
        </div>
      );
  }
}
