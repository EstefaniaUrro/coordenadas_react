import React, { Component } from "react";
import "./App.css";
import Formulario from "./Formulario";
import Coordenadas from "./Coordenadas";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      verFormulario: false,
      verCoordenadas: false,
    };
  }

  formu = (e) => this.setState({ verFormulario: !this.state.verFormulario });
  coor = (e) => this.setState({ verCoordenadas: !this.state.verCoordenadas });

  render() {
    return (
      <div className="App">
        <h1>Coordenadas</h1>
        <br />
        <button
          type="submit"
          className="btn btn-secondary"
          onClick={this.formu}>Formulario</button>
        <button
          type="submit"
          className="btn btn-info"
          onClick={this.coor}>Coordenadas</button>

        {this.state.verFormulario ? <Formulario /> : null}
        {this.state.verCoordenadas ? <Coordenadas /> : null}
      </div>
    );
  }
}

export default App;
